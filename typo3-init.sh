#!/bin/bash

test -e /apache-extra-config  || mkdir /apache-extra-config


    #########
    echo -n HTPASS:
    test -f /var/htpass/.htpasswd && grep "^${TOKEN_USER}" /var/htpass/.htpasswd || htpasswd -bBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" 
    [ ! -z ${TOKEN_USER} ] && [  ! -z ${TOKEN_PASS} ] && ( echo "HTPASS-GEN"
    failed=no
    
    if [ -z ${TOKEN_USER} ] ;then failed=yes;fi
    if [ -z ${TOKEN_PASS} ] ;then failed=yes;fi
    
    echo "$failed" |grep -q yes && [ -z "$(cat /var/htpass/.htpasswd)" ] && ( echo "TOKEN_PASS OR TOKEN_USER MISSING , ,HTACCESS EMPTY generating RANDOM HTPASSWD ";TOKEN_USER="$(cat /dev/urandom |tr -cd '[:alnum:]_\-'  |head -c 10)" ;TOKEN_PASS="$(cat /dev/urandom |tr -cd '[:alnum:]_\-,.'  |head -c 24)"  ; htpasswd -cbBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" )
    [ -z "$(cat /var/htpass/.htpasswd)" ] && echo "$failed" |grep -q yes ||   (echo "EMPTY HTPASSWD GENERATING HTPASSWD FROM ENV"; htpasswd -cbBC 10 /var/htpass/.htpasswd "${TOKEN_USER}" "${TOKEN_PASS}" )

)

## make admin work everywhere 
[ -z ${TOKEN_GUEST_PASS} ] ||  ( cat /var/htpass/.htpasswd > /var/htpass/.htpasswd.user )

[ ! -z ${TOKEN_GUEST_PASS} ] && {

    echo "HTPASS-GUEST-GEN ( username : users )"
    test -f /var/htpass/.htpasswd || (echo "GENERATING HTPASSWD FROM ENV"; htpasswd -bBC 10 /var/htpass/.htpasswd.user "users" "${TOKEN_GUEST_PASS}" )
    test -f /var/htpass/.htpasswd && ( cat /var/htpass/.htpasswd > /var/htpass/.htpasswd.user ;(echo "GENERATING HTPASSWD FROM ENV"; htpasswd -bBC 10 /var/htpass/.htpasswd.user "users" "${TOKEN_GUEST_PASS}" ) )
    }

    # echo "ssh keys"
     touch /var/www/.ssh/authorized_keys && chown root:root /var/www/.ssh /var/www/.ssh/authorized_keys && chmod go-rw  /root/.ssh/authorized_keys /root/.ssh /var/www/.ssh /var/www/.ssh/authorized_keys
    ## now apache fixes
    for myconf in /etc/apache2/sites-available.default/000-default.conf  /etc/apache2/sites-available.default/default-ssl.conf  /etc/apache2/sites-enabled/000-default.conf  /etc/apache2/sites-enabled/default-ssl.conf  ;do
    
    #merge file content
    #ls -1 ${myconf}|grep -q ${myconf} && grep -q "TYPO3 - Block access to miscellaneous protected files" "${myconf}" ||  sed '/\(.\+\)DocumentRoot\(.\+\)/ r /typofixes.conf'  "${myconf}" -i
    ls -1 ${myconf}|grep -q ${myconf} && grep -q "Include /typofixes.conf" "${myconf}" ||  sed 's/\(.\+\)DocumentRoot\(.\+\)/\0\n        Include \/typofixes.conf\n/g'  "${myconf}" -i


done

## preparation stage if no typo3 is found
TYPO_LATEST=$(find /var/www/typo3_src/ -mindepth 1 -maxdepth 1 -type d -name "typo3_src-*" |sed 's/.\+typo3_src-//g'|sort -n|tail -n 1)
echo "LATEST TYPO3 is:"$TYPO_LATEST

test -e /var/wwww/html||mkdir /var/www/html;
chown www-data:www-data /var/www/html
test -e /var/www/html/typo3_src || { echo typo3 not found , doing basic setup
    [ -z "${TYPO_LATEST}" ] && echo "could not find source to install"
    [ -z "${TYPO_LATEST}" ] || {    echo "linking"  "ln -s /var/www/typo3_src/typo3_src-${TYPO_LATEST} /var/www/html/typo3_src";
                                    su -s /bin/bash -c "ln -s /var/www/typo3_src/typo3_src-${TYPO_LATEST} /var/www/html/typo3_src" www-data;
                                    test -e /var/www/html/index.php || su -s /bin/bash -c "cd /var/www/html ;ln -s typo3_src/index.php index.php" www-data;
                                    test -e /var/www/html/typo3     || su -s /bin/bash -c "cd /var/www/html ;ln -s typo3_src/typo3 typo3" www-data;
                                    echo "install finished" ; } ;
    su -s/bin/bash -c "touch /var/www/html/FIRST_INSTALL" www-data
    echo -n ; } ;
    
for phpconf in $(find $(find /etc/ -maxdepth 1 -name "php*") -name php.ini |grep -e apache -e fpm);do
       grep "xdebug.max_nesting_level = 400" "${phpconf}"                || ( echo "xdebug.max_nesting_level = 400"   |tee -a "${phpconf}" )
done


which redis-server && ( echo "setting up redis sessionstorage";
    for phpconf in $(find $(find /etc/ -maxdepth 1 -name "php*") -name php.ini |grep -e apache -e fpm);do
       grep "session.save_handler = redis" "${phpconf}"                || ( echo "session.save_handler = redis"   |tee -a "${phpconf}" )
       grep 'session.save_path = "tcp://127.0.0.1:6379"'  "${phpconf}" || ( echo 'session.save_path = "tcp://127.0.0.1:6379"' |tee -a "${phpconf}" )
    done
    )

which redis-server || ( echo "no redis found;disabling redis session storage";
    for phpconf in $(find $(find /etc/ -maxdepth 1 -name "php*") -name php.ini |grep -e apache -e fpm);do
        sed 's/session.save_path.\+tcp.\+:6379.\+//g' "${phpconf}"  -i
        sed 's/session.save_handler = redis//g' "${phpconf}" -i
        grep -q "xdebug.max_nesting_level = 400" "${phpconf}" || echo "xdebug.max_nesting_level = 400" >> "${phpconf}"
    done
    ) &
###### BACKGROUND TASK

## enable curl
(sleep 120; sed 's/curl_exec\(,\|$\)//g;s/curl_multi_exec\(,\|$\)//g' /etc/php/*/fpm/pool.d/www.conf  -i >/dev/null  ) &

##


ln -s /usr/bin/msmtp /usr/sbin/sendmail.real
wget -q -c https://gitlab.com/the-foundation/msmtp-cron-sendmail/-/raw/master/sendmail -O /tmp/sendmail
sed 's/\r//' /tmp/sendmail -i
grep -q -e 'test -e /etc/sendmail.msmtp.conf' /tmp/sendmail && grep -q -e 'exit "$Status"' /tmp/sendmail && chmod +x /tmp/sendmail ;
mv /tmp/sendmail /usr/sbin/sendmail
